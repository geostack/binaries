This repository contains binary resources for building and testing OpenCL applications.

This includes:
* The now-discontinued AMD APP SDK v2.9.1. This SDK is currently the only CPU-based OpenCL implementation to work on all CPU devices. The SDK was downloaded from [here](https://github.com/ghostlander/AMD-APP-SDK/releases).
* The replacement header and skeleton library. The SDK was downloaded from [here](https://github.com/GPUOpen-LibrariesAndSDKs/OCL-SDK/releases)
* The clinfo tool. This was downloaded in binary form from [here](https://github.com/Oblomov/clinfo)